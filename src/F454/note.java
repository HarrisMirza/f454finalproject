package F454;

class note {
    //Declare Variables
    String Name;
    final int Octave;
    private int NaturalNoteIndex;
    int SharpFlatNoteIndex;


    //Declare Getters and Setters, so we can access the variables
    private int getOctave() {
        return Octave;
    }

    public String getName() {
        return Name;
    }

    public int getNaturalNoteIndex() {
        return NaturalNoteIndex;
    }

    public int getSharpFlatNoteIndex() {
        return SharpFlatNoteIndex;
    }

    //Declare constructors
    public note(String name, int octave) {
        this.Name = name;
        this.Octave = octave;
        calculateNoteIndex(this.Name);
    }

    public note(int MIDINumber) {
        //Calculating Name from MIDI Input
        //MIDINumber is number of note, C0 is 0, D0 is 1, etc...
        //First Find Octave
        this.Octave = Math.floorDiv(MIDINumber, 12);
        //Remove previous octaves to find note
        MIDINumber -= (this.Octave * 12);
        //Find Name
        switch (MIDINumber) {
            case 0:
                this.Name = "C";
                break;
            case 1:
                this.Name = "C#/Db";
                break;
            case 2:
                this.Name = "D";
                break;
            case 3:
                this.Name = "D#/Eb";
                break;
            case 4:
                this.Name = "E";
                break;
            case 5:
                this.Name = "F";
                break;
            case 6:
                this.Name = "F#/Gb";
                break;
            case 7:
                this.Name = "G";
                break;
            case 8:
                this.Name = "G#/Ab";
                break;
            case 9:
                this.Name = "A";
                break;
            case 10:
                this.Name = "A#/Bb";
                break;
            case 11:
                this.Name = "B";
                break;
        }
        calculateNoteIndex(this.Name);
    }

    int getMidiNumber() {
        int midiNumber = 0;
        //Add Octave
        midiNumber += (this.Octave * 12);

        //Add Name
        switch (this.Name) {
            case "C":
                midiNumber += 0;
                break;
            case "C#/Db":
                midiNumber += 1;
                break;
            case "D":
                midiNumber += 2;
                break;
            case "D#/Eb":
                midiNumber += 3;
                break;
            case "E":
                midiNumber += 4;
                break;
            case "F":
                midiNumber += 5;
                break;
            case "F#/Gb":
                midiNumber += 6;
                break;
            case "G":
                midiNumber += 7;
                break;
            case "G#/Ab":
                midiNumber += 8;
                break;
            case "A":
                midiNumber += 9;
                break;
            case "A#/Bb":
                midiNumber += 10;
                break;
            case "B":
                midiNumber += 11;
                break;
        }
        //Return Output
        return midiNumber;
    }

    //Other Methods
    //Sets NoteIndex from Name
    private void calculateNoteIndex(String note) {
        switch (note) {
            //Calculates Name Index
            case "C":
                this.NaturalNoteIndex = 1;
                this.SharpFlatNoteIndex = 0;
                break;
            case "C#/Db":
                this.NaturalNoteIndex = 0;
                this.SharpFlatNoteIndex = 1;
                break;
            case "D":
                this.NaturalNoteIndex = 2;
                this.SharpFlatNoteIndex = 0;
                break;
            case "D#/Eb":
                this.NaturalNoteIndex = 0;
                this.SharpFlatNoteIndex = 2;
                break;
            case "E":
                this.NaturalNoteIndex = 3;
                this.SharpFlatNoteIndex = 0;
                break;
            case "F":
                this.NaturalNoteIndex = 4;
                this.SharpFlatNoteIndex = 0;
                break;
            case "F#/Gb":
                this.NaturalNoteIndex = 0;
                this.SharpFlatNoteIndex = 3;
                break;
            case "G":
                this.NaturalNoteIndex = 5;
                this.SharpFlatNoteIndex = 0;
                break;
            case "G#/Ab":
                this.NaturalNoteIndex = 0;
                this.SharpFlatNoteIndex = 4;
                break;
            case "A":
                this.NaturalNoteIndex = 6;
                this.SharpFlatNoteIndex = 0;
                break;
            case "A#/Bb":
                this.NaturalNoteIndex = 0;
                this.SharpFlatNoteIndex = 5;
                break;
            case "B":
                this.NaturalNoteIndex = 7;
                this.SharpFlatNoteIndex = 0;
                break;
            default:
                this.NaturalNoteIndex = 0;
                this.SharpFlatNoteIndex = 0;
                break;
        }
    }

    //Compares 2 Notes
    public static int Compare(note Note1, note Note2) {
        int match = 0;
        //Returns 1 if Octave Matches,
        //2 if the Name Matches,
        //3 for a complete match
        //and 0 for no match
        if (Note1.getOctave() == Note2.getOctave()) {
            match += 1;
        }
        if (Note1.getName().equals(Note2.getName())) {
            match += 2;
        }
        return match;
    }

    //Returns String representation of Name
    @Override
    public String toString() {
        return (this.Name + this.Octave);
    }
}
