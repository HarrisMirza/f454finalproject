package F454;

import javax.sound.midi.MidiDevice;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.Hashtable;

class options extends JFrame {

    private int midiInputIndex = 0;
    private int midiOutputIndex = 0;
    private static int midiPollingTime = 10;
    private boolean closeWithoutSaving;
    private final JComboBox<String> midiInputBox;
    private final JComboBox<String> midiOutputBox;

    public options(boolean midiAvailable) {
        //Set Main UI Attributes
        setTitle("Options");
        setLayout(new FlowLayout());
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        //Setup Panels
        JPanel mainOptionPanel = new JPanel();
        mainOptionPanel.setLayout(new BoxLayout(mainOptionPanel, BoxLayout.Y_AXIS));
        add(mainOptionPanel);
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        add(buttonPanel);

        //Setup MIDI Variables
        ArrayList<MidiDevice.Info> midiInputDevices;
        ArrayList<MidiDevice.Info> midiOutputDevices;
        if (midiAvailable) {
            //Get List of Input Devices only if they are available
            midiInputDevices = mainUI.mainMIDI.midiInputDevices;
            //Get List of Output Devices only if they are available
            midiOutputDevices = mainUI.mainMIDI.midiOutputDevices;
        } else {
            //Otherwise set the lists to blank ArrayList
            midiInputDevices = new ArrayList<>();
            midiOutputDevices = new ArrayList<>();
        }
        //Setup UI Components
        //midiInputBox and Label
        JLabel midiInputBoxLabel = new JLabel("Midi Input Devices");
        midiInputBoxLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainOptionPanel.add(midiInputBoxLabel);
        //Get Names of Input Devices
        String[] midiInputDeviceNames = new String[midiInputDevices.size() + 1];
        if (midiAvailable) {
            for (int inputDeviceIndex = 0; inputDeviceIndex <= midiInputDeviceNames.length; inputDeviceIndex++) {
                midiInputDeviceNames[inputDeviceIndex] = midiInputDevices.get(inputDeviceIndex).getName();
            }
        } else {
            //Otherwise add 'No Midi Devices' to the list of names
            midiInputDeviceNames[0] = "No MIDI Devices";
        }
        //Setup the Input Device selection box.
        midiInputBox = new JComboBox<>(midiInputDeviceNames);
        mainOptionPanel.add(midiInputBox);
        mainOptionPanel.add(Box.createRigidArea(new Dimension((this.getWidth()), 20)));

        //midiOutputBox and Label
        JLabel midiOutputBoxLabel = new JLabel("Midi Output Devices");
        midiOutputBoxLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainOptionPanel.add(midiOutputBoxLabel);
        //Get Names of Output Devices
        String[] midiOutputDeviceNames = new String[midiOutputDevices.size() + 1];
        if (midiAvailable) {
            for (int inputDeviceIndex = 1; inputDeviceIndex <= midiOutputDeviceNames.length; inputDeviceIndex++) {
                midiOutputDeviceNames[inputDeviceIndex] = midiOutputDevices.get(inputDeviceIndex).getName();
            }
        } else {
            //Otherwise add 'No Midi Devices' to the list of names
            midiOutputDeviceNames[0] = "No MIDI Devices";
        }
        //Setup the Output Device selection box.
        midiOutputBox = new JComboBox<>(midiOutputDeviceNames);
        mainOptionPanel.add(midiOutputBox);
        mainOptionPanel.add(Box.createRigidArea(new Dimension((this.getWidth()), 20)));

        //midiPollingTimeSlider
        JLabel midiPollingTimeSliderLabel = new JLabel("Midi Polling Time : " + "10ms");
        midiPollingTimeSliderLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        mainOptionPanel.add(midiPollingTimeSliderLabel);
        //Setting Main Attributes, Orientation, Minimum, Maximum and Default Values
        JSlider midiPollingTimeSlider = new JSlider(JSlider.HORIZONTAL, 1, 200, 10);
        midiPollingTimeSlider.setPreferredSize(new Dimension(300, 50));
        //Setting Tick Spacing
        midiPollingTimeSlider.setMajorTickSpacing(50);
        midiPollingTimeSlider.setMinorTickSpacing(10);
        midiPollingTimeSlider.setPaintTicks(true);
        midiPollingTimeSlider.setPaintLabels(true);
        //Setup Slider Labels
        Hashtable<Integer, JLabel> labelTable = new Hashtable<>();
        labelTable.put(1, new JLabel("1 ms"));
        labelTable.put(50, new JLabel("50 ms"));
        labelTable.put(100, new JLabel("100 ms"));
        labelTable.put(150, new JLabel("150 ms"));
        labelTable.put(200, new JLabel("200 ms"));
        midiPollingTimeSlider.setLabelTable(labelTable);
        mainOptionPanel.add(midiPollingTimeSlider);
        mainOptionPanel.add(Box.createRigidArea(new Dimension((this.getWidth()), 20)));

        //confirmButton
        JButton confirmButton = new JButton("Ok");
        buttonPanel.add(confirmButton);
        buttonPanel.add(Box.createRigidArea(new Dimension(10, this.getHeight())));

        //cancelButton
        JButton cancelButton = new JButton("Cancel");
        buttonPanel.add(cancelButton);

        //Add Event Listeners
        closeWithoutSaving = true;
        this.addComponentListener(new ComponentAdapter() {

            @Override
            public void componentHidden(ComponentEvent componentEvent) {
                if (closeWithoutSaving) {
                    //When the window is hidden, if you don't want to save the options, then set them back to the previous setting
                    midiInputBox.setSelectedIndex(midiInputIndex);
                    midiOutputBox.setSelectedIndex(midiOutputIndex);
                    midiPollingTimeSlider.setValue(midiPollingTime);
                } else {
                    //Otherwise, we will save the options
                    midiInputIndex = midiInputBox.getSelectedIndex();
                    midiOutputIndex = midiOutputBox.getSelectedIndex();
                    midiPollingTime = midiPollingTimeSlider.getValue();
                    mainUI.setMidiPollingTime(midiPollingTime);
                    mainUI.mainMIDI.setMidi(midiInputIndex, midiOutputIndex, midiPollingTime, mainUI.MIDIStarted);
                }
            }
        });

        //MidiPollingTimeSlider
        midiPollingTimeSlider.addChangeListener(e -> {
            //Show Polling Time in Label and ToolTip
            midiPollingTimeSlider.setToolTipText(String.valueOf(midiPollingTimeSlider.getValue()));
            midiPollingTimeSliderLabel.setText("Midi Polling Time : " + midiPollingTimeSlider.getValue() + " ms");
        });

        //confirmButton
        confirmButton.addActionListener(e -> {
            closeWithoutSaving = false;
            this.setVisible(false);
        });

        //cancelButton
        cancelButton.addActionListener(e -> {
            closeWithoutSaving = true;
            this.setVisible(false);
        });
    }

    @Override
    public void update(Graphics g) {
        //When the window is updated, also update the list of devices.
        updateMidiDeviceLists();
        super.update(g);
    }

    private void updateMidiDeviceLists() {
        if (mainUI.MIDIAvailable) {
            //Remove Old Items
            midiInputBox.removeAllItems();
            midiOutputBox.removeAllItems();

            //Get New Devices
            ArrayList<MidiDevice.Info> midiInputDevices = mainUI.mainMIDI.midiInputDevices;
            ArrayList<MidiDevice.Info> midiOutputDevices = mainUI.mainMIDI.midiOutputDevices;

            //Iterate Over them and add the names
            for (MidiDevice.Info midiInputDevice : midiInputDevices) {
                midiInputBox.addItem(midiInputDevice.getName());
            }
            for (MidiDevice.Info midiOutputDevice : midiOutputDevices) {
                midiOutputBox.addItem(midiOutputDevice.getName());
            }
        } else {
            //Remove Old Items
            midiInputBox.removeAllItems();
            midiOutputBox.removeAllItems();
            //Add No Devices Found Message
            midiInputBox.addItem("No MIDI Devices");
            midiOutputBox.addItem("No MIDI Devices");
        }

    }

}
