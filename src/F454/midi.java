package F454;

import javax.sound.midi.*;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

class midi {
    final ArrayList<MidiDevice.Info> midiInputDevices;
    final ArrayList<MidiDevice.Info> midiOutputDevices;
    midiInputReceiver mainMidiInputReceiver;
    private Receiver mainMidiOutputReceiver;
    private MidiDevice.Info currentMidiInputDeviceInfo;
    private MidiDevice.Info currentMidiOutputDeviceInfo;
    private MidiDevice currentMidiInputDevice;
    private MidiDevice currentMidiOutputDevice;
    private ScheduledFuture midiCheckSchedule;

    /**
     * Initialises MIDI devices and sorts them into Input and Output devices.
     * This is the constructor for this class.
     * @throws MidiUnavailableException Throws exception when no MIDI devices are present.
     */
    midi() throws MidiUnavailableException {
        //Initialise Midi Devices
        MidiDevice.Info[] midiDevicesInfo = MidiSystem.getMidiDeviceInfo();
        midiInputDevices = new ArrayList<>();
        midiOutputDevices = new ArrayList<>();
        //Sort between Input and Output devices
        for (MidiDevice.Info midiDeviceInfo : midiDevicesInfo) {
            try {
                MidiDevice device = MidiSystem.getMidiDevice(midiDeviceInfo);
                device.open();
                try {
                    //All Input Devices will have a transmitter
                    if (device.getTransmitter() != null) {
                        midiInputDevices.add(midiDeviceInfo);
                    }
                } catch (MidiUnavailableException e1) {
                    //Do nothing if the device doesn't have a transmitter
                }
                try {
                    //All Output Devices will have a receiver
                    if (device.getReceiver() != null) {
                        midiOutputDevices.add(midiDeviceInfo);
                    }
                } catch (MidiUnavailableException e1) {
                    //Do nothing if the device doesn't have a transmitter
                }
                device.close();
            } catch (MidiUnavailableException e) {
                //This will only occur if a device is unavailable and so no action has to be taken.
            }
        }

        if (midiInputDevices.size() == 0 || midiOutputDevices.size() == 0) {
            //If there is either no input or no output devices, MIDI is unavailable so we throw an exception.
            throw new MidiUnavailableException("No Input or Output devices are available");
        }

        //Set Midi Input Device
        currentMidiInputDeviceInfo = midiInputDevices.get(0);
        //Set Midi Output Device
        currentMidiOutputDeviceInfo = midiInputDevices.get(0);

    }

    /**
     * Starts MIDI I/O, starts checking for input and sets up devices for output.
     *
     * @param midiPollingTime The polling time for the MIDI Input Check
     * @return Returns True if it was successful, and False if it wasn't
     */
    boolean startMidi(int midiPollingTime) {
        try {
            //Try to get Input and Output devices, and  the Receivers where applicable
            currentMidiInputDevice = MidiSystem.getMidiDevice(currentMidiInputDeviceInfo);
            currentMidiOutputDevice = MidiSystem.getMidiDevice(currentMidiOutputDeviceInfo);
            currentMidiInputDevice.open();
            currentMidiOutputDevice.open();
            mainMidiInputReceiver = new midiInputReceiver();
            currentMidiInputDevice.getTransmitter().setReceiver(mainMidiInputReceiver);
            mainMidiOutputReceiver = currentMidiOutputDevice.getReceiver();

        } catch (MidiUnavailableException e) {
            //If this fails, midi isn't started
            return false;
        }
        //Set MIDI Check to run at a regular interval on a new thread.
        try {
            midiCheckSchedule = Executors.newSingleThreadScheduledExecutor().scheduleAtFixedRate(() ->
            {
                //Get Notes from the current chord/scale
                ArrayList<note> currentChordScaleNotes = new ArrayList<>(Arrays.asList(mainUI.getChordScale(mainUI.getSelectedChordScaleName(), mainUI.getChords(), mainUI.getScales())));
                ArrayList<Boolean> noteMatches = new ArrayList<>();

                //If the chord is a scale compare to the last 12 notes.
                if (mainUI.getSelectedChordScaleName().contains("Scale")) {
                    for (note InputNote : mainMidiInputReceiver.last12Notes) {
                        for (note Note : currentChordScaleNotes) {
                            if (note.Compare(InputNote, Note) == 2) {
                                noteMatches.add(true);
                            } else {
                                noteMatches.add(false);
                            }
                        }
                    }
                    //Otherwise compare to the active midi notes.
                } else {
                    for (note InputNote : mainMidiInputReceiver.activeMidiNotes) {
                        for (note Note : currentChordScaleNotes) {
                            if (note.Compare(InputNote, Note) == 2) {
                                noteMatches.add(true);
                            } else {
                                noteMatches.add(false);
                            }
                        }
                    }
                }
                //Count the amount of matching notes.
                int noteMatchCount = 0;
                for (Boolean noteMatch : noteMatches) {
                    if (noteMatch) {
                        noteMatchCount += 1;
                    }
                }
                //If there are enough matching notes, then the chord/scale matches and the label will change
                if (noteMatchCount >= currentChordScaleNotes.size()) {
                    mainUI.matchLabel.setBackground(Color.GREEN);
                    mainUI.matchLabel.setText("You are playing the correct chord/scale!");
                    //If there aren't enough matching notes, then the chord/scale doesn't matches and the label will change
                } else {
                    mainUI.matchLabel.setBackground(Color.RED);
                    mainUI.matchLabel.setText("You are not playing the correct chord/scale!");
                }
            }, 0, midiPollingTime, TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    /**
     * Sets Midi Input and Output Devices, and restarts Midi if it has been started.
     * @param inputIndex The index of the input device.
     * @param outputIndex The index of the output device.
     * @param midiPollingTime The polling time of the MIDI input check.
     * @param midiStarted True if MIDI has been started.
     */
    void setMidi(int inputIndex, int outputIndex, int midiPollingTime, boolean midiStarted) {
        //Set Midi Input Device
        currentMidiInputDeviceInfo = midiInputDevices.get(inputIndex);

        //Set Midi Output Device
        currentMidiOutputDeviceInfo = midiOutputDevices.get(outputIndex);

        if (midiStarted) {
            //Restart midi if it is already started.
            this.stopMidi();
            startMidi(midiPollingTime);
        }
    }

    /**
     * Closes MIDI devices and cancels the check.
     */
    void stopMidi() {
        //Close devices and stop the Midi Check
        currentMidiInputDevice.close();
        currentMidiOutputDevice.close();
        midiCheckSchedule.cancel(true);
    }

    /**
     * Sends MIDI Notes to output device. Opens a sequencer and adds the notes to a track,
     * then sends the notes to the device.
     *
     * @param chordScaleName The name of the Chord or Scale that is being sent.
     * @param chordScaleNotes The notes of the Chord or Scale that is being sent.
     */
    boolean sendMidiChordScale(String chordScaleName, note[] chordScaleNotes) {
        try {
            //Get Sequencer and Set Receiver
            Sequencer sequencer = MidiSystem.getSequencer();
            sequencer.open();
            sequencer.getTransmitter().setReceiver(mainMidiOutputReceiver);
            //Build Sequence
            Sequence sequence = new Sequence(Sequence.PPQ, 2);
            Track mainTrack = sequence.createTrack();

            //Check Notes are Valid
            for (note chordScaleNote : chordScaleNotes) {
                //If the Midi Number isn't between 0 and 127 return false and dont send the message.
                if(chordScaleNote.getMidiNumber() < 0 || chordScaleNote.getMidiNumber() > 127 )
                {
                    sequencer.close();
                    return false;
                }
            }
            int tick;
            //First Play Each Note Individually
            for (tick = 0; tick < (chordScaleNotes.length * 2); tick += 2) {
                //For Each Note
                //Add Note On Event
                mainTrack.add(new MidiEvent(new ShortMessage(ShortMessage.NOTE_ON, 0, chordScaleNotes[tick / 2].getMidiNumber(), 90), tick));
                //Add Note Off Event
                mainTrack.add(new MidiEvent(new ShortMessage(ShortMessage.NOTE_OFF, 0, chordScaleNotes[tick / 2].getMidiNumber(), 90), tick + 1));
            }
            //Increment Tick for next note
            tick += 1;

            //If it isn't a scale, play the chord all at once;
            if (!chordScaleName.contains("Scale")) {
                //Play the whole chord at once
                for (note note : chordScaleNotes) {
                    //For Each Name
                    //Add Name On Event
                    mainTrack.add(new MidiEvent(new ShortMessage(ShortMessage.NOTE_ON, 0, note.getMidiNumber(), 90), tick));
                    //Add Name Off Event
                    mainTrack.add(new MidiEvent(new ShortMessage(ShortMessage.NOTE_OFF, 0, note.getMidiNumber(), 90), tick + 2));
                }
            }

            //Play Sequence
            sequencer.setSequence(sequence);
            sequencer.start();
        } catch (Exception e) {
            return false;
        }
        return true;
    }
}


class midiInputReceiver implements Receiver {
    final ArrayList<note> activeMidiNotes;
    final fixedSizeQueue<note> last12Notes;

    /**
     * Initialises relevant variables for the MIDI Input Reciever
     *
     * @see Receiver
     */
    midiInputReceiver() {
        //Initialise Arrays/Queues
        activeMidiNotes = new ArrayList<>();
        last12Notes = new fixedSizeQueue<>(12);
    }

    /**
     * This method is called when the reciever gets MIDI Input.
     * It decodes the message and sets the value of the Active Midi Notes and the Last 12 Notes.
     *
     * @param midiMessage The message that has been recieved.
     * @param l The timestamp of the Midi Message
     */
    @Override
    public void send(MidiMessage midiMessage, long l) {
        //This is run whenever a midi message is received
        //Get the message in an array
        byte[] message = midiMessage.getMessage();
        if ((int) message[0] == -112) {
            //If the message is a 'Note On' event, then add it to the Array/Queue
            note input = new note((int) message[1]);
            activeMidiNotes.add(input);
            last12Notes.add(input);
        } else if ((int) message[0] == -128) {
            //If the message is a 'Note Off' event, then remove it from the array
            activeMidiNotes.removeIf(Note -> (note.Compare(Note, new note((int) message[1]))) == 3);
        }
        try {
            //Update the pianoPanel so that any active notes can be displayed.
            SwingUtilities.invokeAndWait(() -> mainUI.pianoPanel.repaint());

        } catch (Exception e) {
            System.out.println("Error");
        }
    }

    @Override
    public void close() {
    }
}

class fixedSizeQueue<K> extends LinkedList<K> {
    private final int maxSize;

    /**
     * This Initialises the Maximum Size of the queue
     * @param size The maximum size of the Queue
     */
    //Initialise the Maximum Size
    fixedSizeQueue(int size) {
        this.maxSize = size;
    }

    /**
     * Adds a value to the queue
     * @param k The value that you want to add.
     * @return Returns true if the value was added successfully, otherwise it returns full.
     * @see LinkedList
     */
    public boolean add(K k) {
        //When an element is added
        boolean result = super.add(k);
        //If it is greater than the maximum size
        if (this.size() > maxSize) {
            //Then remove the front (Oldest) element
            super.removeFirst();
        }
        return result;
    }
}
