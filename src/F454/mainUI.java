package F454;

import javax.imageio.ImageIO;
import javax.sound.midi.MidiUnavailableException;
import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

class mainUI extends JFrame {

    //Declare Public Variables
    private static LinkedHashMap<String, note[]> chords;
    private static LinkedHashMap<String, note[]> scales;
    private static String selectedChordScaleName;
    static ArrayList<String> rootNotes;
    static boolean MIDIStarted;
    static boolean MIDIAvailable = false;
    static midi mainMIDI;
    static JLabel matchLabel;
    static JPanel pianoPanel;
    private final JButton midiToggleButton;
    private static JFrame mainOptions;
    private static int midiPollingTime;

    /**
     * Draws the Main User Interface of the program and configures attributes of UI element.
     * Also contains the event listeners for buttons and lists.
     * Is the Constructor for this class.
     *
     * @see JFrame
     */
    private mainUI() {
        //Initialise Program
        initProgram();

        //Set Main UI Attributes
        setTitle("Chord/Scale Library");
        setSize(900, 480);
        setLayout(new FlowLayout());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        //Add Icon
        try
        {
            ArrayList<Image> Icons = new ArrayList<>();
            //Load every icon
            //1024
            Icons.add(ImageIO.read(new File("res/icon-1024.png")));
            //512x512
            Icons.add(ImageIO.read(new File("res/icon-512.png")));
            //256x256
            Icons.add(ImageIO.read(new File("res/icon-256.png")));
            //128x128
            Icons.add(ImageIO.read(new File("res/icon-128.png")));
            //64x64
            Icons.add(ImageIO.read(new File("res/icon-64.png")));
            //32x32
            Icons.add(ImageIO.read(new File("res/icon-32.png")));
            //16x16
            Icons.add(ImageIO.read(new File("res/icon-16.png")));
            this.setIconImages(Icons);
        }catch (IOException e)
        {
            //Error loading icons
        }

        //Setup Panels
        //Add Main List Panel for Root Note and Chord/Scale Lists
        JPanel mainListPanel = new JPanel();
        mainListPanel.setLayout(new FlowLayout());
        add(mainListPanel);
        //Add Chord Scale List Panel for Chord/Scale Lists
        JPanel chordScaleListPanel = new JPanel();
        chordScaleListPanel.setLayout(new BoxLayout(chordScaleListPanel, BoxLayout.Y_AXIS));
        //Add Piano Panel to display the virtual keyboard
        pianoPanel = new pianoDisplay();
        pianoPanel.setPreferredSize(new Dimension(400, this.getHeight() - 150));
        add(pianoPanel);
        //Add Button Panel to hold buttons.
        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.setAlignmentX(JPanel.CENTER_ALIGNMENT);
        buttonPanel.setPreferredSize(new Dimension(700, 60));
        add(buttonPanel);

        //Setup UI Components
        //Setup Root Notes List
        JList<String> rootNotesList = new JList<>(rootNotes.toArray(new String[12]));
        //Initialise list of chords for Chord List using C as the initial root note
        ArrayList<String> chordArrayList = getRootNoteChords("C");
        JList<String> chordList = new JList<>(chordArrayList.toArray(new String[chordArrayList.size()]));
        //Initialise list of scales for Scale List using C as the initial root note
        ArrayList<String> scaleArrayList = getRootNoteScales("C");
        JList<String> scaleList = new JList<>(scaleArrayList.toArray(new String[scaleArrayList.size()]));
        //Setup buttons
        JButton optionsButton = new JButton("Options");
        midiToggleButton = new JButton("Start MIDI");
        JButton midiResetCheckButton = new JButton("Reset MIDI Check");
        JButton playButton = new JButton("Play");


        //Configure UI Components (Set attributes such as size, initial selection, etc... and also add action listeners)
        //Main List Panel
        //Root Notes List
        rootNotesList.setSelectedIndex(0);
        rootNotesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        rootNotesList.setLayoutOrientation(JList.VERTICAL);
        rootNotesList.setVisibleRowCount(-1);
        //Add Scroll Pane to Root Name List
        JScrollPane rootNotesListScrollPane = new JScrollPane(rootNotesList);
        rootNotesListScrollPane.setPreferredSize(new Dimension(150, 320));
        rootNotesListScrollPane.createVerticalScrollBar();
        mainListPanel.add(rootNotesListScrollPane);
        //Add an Event Listener to Root Name List
        rootNotesList.addListSelectionListener(e ->
        {
            //When a new root note is selected, update the contents of both the chord and scale lists and select the first element of the Chord List
            ArrayList<String> chordArrayList1 = getRootNoteChords(rootNotesList.getSelectedValue());
            chordList.setListData(chordArrayList1.toArray(new String[chordArrayList1.size()]));

            ArrayList<String> scaleArrayList1 = getRootNoteScales(rootNotesList.getSelectedValue());
            scaleList.setListData(scaleArrayList1.toArray(new String[scaleArrayList1.size()]));

            chordList.setSelectedIndex(0);
        });

        //Chord List
        chordList.setSelectedIndex(0);
        chordList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        chordList.setLayoutOrientation(JList.VERTICAL);
        chordList.setVisibleRowCount(-1);
        //Add Scroll Pane to Chord List
        JScrollPane chordListScrollPane = new JScrollPane(chordList);
        chordListScrollPane.setPreferredSize(new Dimension(150, 160));
        chordListScrollPane.createVerticalScrollBar();
        chordScaleListPanel.add(chordListScrollPane);
        //Add Event Listener to Chord List
        chordList.addListSelectionListener(e ->
        {
            //When Chord List Selection Changes, if the value isn't -1 (meaning no item is selected),
            //then clear the selection of Scale List (which sets the index to -1)
            if (chordList.getSelectedIndex() != -1) {
                scaleList.clearSelection();
                mainUI.setSelectedChordScaleName(chordList.getSelectedValue());
                pianoPanel.paint(pianoPanel.getGraphics());
            }
        });
        //Scale List
        scaleList.setSelectedIndex(-1);
        scaleList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        scaleList.setLayoutOrientation(JList.VERTICAL);
        scaleList.setVisibleRowCount(-1);
        //Add Scroll Pane to Scale List
        JScrollPane scaleListScrollPane = new JScrollPane(scaleList);
        scaleListScrollPane.setPreferredSize(new Dimension(150, 160));
        scaleListScrollPane.createVerticalScrollBar();
        chordScaleListPanel.add(scaleListScrollPane);
        //Add Event Listener to Chord List
        scaleList.addListSelectionListener(e ->
        {
            //When Scale List Selection Changes, if the value isn't -1 (meaning no item is selected),
            //then clear the selection of Chord List (which sets the index to -1)
            if (scaleList.getSelectedIndex() != -1) {
                chordList.clearSelection();
                mainUI.setSelectedChordScaleName(scaleList.getSelectedValue());
                pianoPanel.paint(pianoPanel.getGraphics());
            }
        });
        //Add ChordScaleListPanel
        mainListPanel.add(chordScaleListPanel);

        //Button Panel
        //Options Button
        buttonPanel.add(optionsButton);
        //Add Action Listener to Options Button
        optionsButton.addActionListener(e -> mainOptions.setVisible(true));
        //MIDI Toggle Button
        buttonPanel.add(midiToggleButton);
        //Setup Variable for MIDI Start Stop
        MIDIStarted = false;
        //Add Action Listener to Midi Toggle Button
        midiToggleButton.addActionListener(e ->
        {
            //When midi toggle button is pressed...
            if (!MIDIStarted) {
                //If midi isn't started already, try to start it
                boolean success = mainMIDI.startMidi(midiPollingTime);
                if (success) {
                    //If it is successful, change the button text to stop midi, and enable the play and midi check reset buttons.
                    midiToggleButton.setText("Stop MIDI");
                    playButton.setEnabled(true);
                    midiResetCheckButton.setEnabled(true);
                    MIDIStarted = true;
                } else {
                    JOptionPane.showMessageDialog(this, "Error starting MIDI. Please ensure the MIDI Devices are available and selected in options.", "MIDI Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                //If midi is started, then stop it, change the button text to start midi, and disable the play and midi check reset buttons.
                mainMIDI.stopMidi();
                midiToggleButton.setText("Start MIDI");
                playButton.setEnabled(false);
                midiResetCheckButton.setEnabled(false);
                MIDIStarted = false;
            }
        });
        //MidiCheckReset Button
        buttonPanel.add(midiResetCheckButton);
        midiResetCheckButton.setEnabled(false);
        midiResetCheckButton.addActionListener(e ->
        {
            //This button resets the midi check by resetting the last notes played and the active notes lists.
            mainMIDI.mainMidiInputReceiver.last12Notes.clear();
            mainMIDI.mainMidiInputReceiver.activeMidiNotes.clear();
        });
        //Play Button
        playButton.setEnabled(false);
        buttonPanel.add(playButton);
        //Add Action Listener to Play Button
        playButton.addActionListener(e -> {
            note[] notes = getChordScale(selectedChordScaleName, chords, scales);
            //On a new thread, play the notes of the current chord/scale
            Future<?> future = Executors.newSingleThreadExecutor().submit(() -> mainMIDI.sendMidiChordScale(selectedChordScaleName, notes));
            //After it is completed, try to get what the method returned and display an error if there is a problem.
            try {
                if(future.get().equals(false) )
                {
                    throw new Exception();
                }
            } catch (Exception e1) {
                JOptionPane.showMessageDialog(new JFrame(), "Error Playing Midi Sound, Please ensure that the correct devices have been selected in the options, and that the device isn't in use", "MIDI Out Error", JOptionPane.ERROR_MESSAGE);
            }
        });

        //Configure match label to check if there is a chord or scale match.
        matchLabel = new JLabel("You are not playing the correct chord/scale!");
        matchLabel.setBackground(Color.RED);
        matchLabel.setOpaque(true);
        buttonPanel.add(matchLabel);
    }

    /**
     * Main method of the program, initialises the Main UI, the Options menu and makes the Main UI Visible.
     *
     * @param args The command line arguments from when the program runs.
     */
    public static void main(String[] args) {
        //Initialise Options UI, and make invisible
        mainOptions = new options(MIDIAvailable);
        mainOptions.setSize(new Dimension(350, 350));
        mainOptions.setVisible(false);

        //Initialise Main UI, and make visible
        JFrame mainJFrame = new mainUI();
        mainJFrame.setVisible(true);

        //Update the Options menu with the new midi devices from initProgram().
        mainOptions.update(mainOptions.getGraphics());
    }

    /**
     * Initialises variables that are neccessary for the program to run.
     */
    private void initProgram() {
        //Initialise Public Variables
        selectedChordScaleName = "C Major";
        rootNotes = new ArrayList<>();
        rootNotes.add("C");
        rootNotes.add("C#/Db");
        rootNotes.add("D");
        rootNotes.add("D#/Eb");
        rootNotes.add("E");
        rootNotes.add("F");
        rootNotes.add("F#/Gb");
        rootNotes.add("G");
        rootNotes.add("G#/Ab");
        rootNotes.add("A");
        rootNotes.add("A#/Bb");
        rootNotes.add("B");
        chords = new LinkedHashMap<>();
        scales = new LinkedHashMap<>();

        //Initialise MIDI
        try {
            mainMIDI = new midi();
            MIDIAvailable = true;
        } catch (MidiUnavailableException e) {
            JOptionPane.showMessageDialog(this, "MIDI is unavailable, some features will not function. Please ensure all MIDI devices are connected and try restarting the program.", "MIDI Error", JOptionPane.ERROR_MESSAGE);
        }

        //Get Chords and Scales
        try {
            generateChordsScales();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Error Loading Chord and Scale patterns, please try reinstalling the program.", "IO Error", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }

        //Set initial MIDI Polling Time
        midiPollingTime = 10;

    }

    /**
     * Opens the Chord and Scale pattern files and generates the chords and scales. These are then stored.
     *
     * @throws IOException Throws exception when there is a problem opening or reading the pattern files.
     * @see note
     */

    private void generateChordsScales() throws IOException {
        //Open Chord Pattern File, and create a reader
        FileInputStream ChordInputStream = new FileInputStream("res/ChordPatterns.txt");
        BufferedReader ChordBufferedReader = new BufferedReader(new InputStreamReader(ChordInputStream));

        //Declare Variables
        String[] ChordFileLine;
        Map<String, int[]> ChordPatterns = new LinkedHashMap<>();

        //Iterate Over Lines, while the next line isn't empty
        ChordFileLine = ChordBufferedReader.readLine().split("=");
        while (ChordFileLine.length == 2) {
            //Each Line of the file has the name of the pattern and
            //the integer notation of the chord
            //e.g. Major=1,5,8
            //Get Pattern Name and Notes{
            String ChordPatternName = ChordFileLine[0];
            String[] ChordPatternNotes = ChordFileLine[1].split(",");
            //Convert the Strings to Integers
            int[] ChordPatternNoteNumbers = Arrays.stream(ChordPatternNotes).mapToInt(Integer::parseInt).toArray();
            ChordPatterns.put(ChordPatternName, ChordPatternNoteNumbers);
            //Read Next Line
            try {
                ChordFileLine = ChordBufferedReader.readLine().split("=");
            } catch (NullPointerException e) {
                break;
            }
        }
        //Close Chord Pattern File
        ChordBufferedReader.close();
        ChordInputStream.close();

        //Open Scale Pattern File, and create a reader
        FileInputStream ScaleInputStream = new FileInputStream("res/ScalePatterns.txt");
        BufferedReader ScaleBufferedReader = new BufferedReader(new InputStreamReader(ScaleInputStream));

        //Declare Variables
        String[] ScaleFileLine;
        Map<String, int[]> ScalePatterns = new LinkedHashMap<>();

        //Iterate Over Lines, while the next line isn't empty
        ScaleFileLine = ScaleBufferedReader.readLine().split("=");
        while (ScaleFileLine.length == 2) {
            //Each Line of the file has the name of the pattern and
            //the integer notation of the chord
            //e.g. Major Scale =1,3,5,8,10,12,13
            //Get Pattern Name and Notes
            String ScalePatternName = ScaleFileLine[0];
            String[] ScalePatternNotes = ScaleFileLine[1].split(",");
            //Convert the Strings to Integers
            int[] ScalePatternNoteNumbers = Arrays.stream(ScalePatternNotes).mapToInt(Integer::parseInt).toArray();
            ScalePatterns.put(ScalePatternName, ScalePatternNoteNumbers);
            //Read Next Line
            try {
                ScaleFileLine = ScaleBufferedReader.readLine().split("=");
            } catch (NullPointerException e) {
                break;
            }

        }
        //Close Scale Pattern File
        ScaleBufferedReader.close();
        ScaleInputStream.close();


        //Get Chords and Scales for each Root Name
        //Iterate over root notes
        for (String rootNote : rootNotes) {
            //CHORDS
            //Iterate over Keys in Patterns map
            for (String patternName : ChordPatterns.keySet()) {
                //Set Name of Chord to root note + Key
                String ChordName = rootNote + " " + patternName;
                //Get Int Array of Notes
                int[] pattern = ChordPatterns.get(patternName);
                //Get Index of the Root Name
                int rootNoteIndex = rootNotes.indexOf(rootNote);
                //Declare Name Array for storing Notes
                note[] chordNotes = new note[pattern.length];
                //Iterate over notes in pattern
                for (int i = 0; i < pattern.length; i++) {
                    //Make a MIDI number starting at Octave 3 for the note by adding the index of the root note to the
                    //pattern number (-1 to start at 0 index) and adding 36 for 3 octaves
                    chordNotes[i] = new note(((pattern[i] - 1) + rootNoteIndex) + 36);

                }
                chords.put(ChordName, chordNotes);
            }

            //SCALES
            //Iterate over Keys in Patterns map
            for (String patternName : ScalePatterns.keySet()) {
                //Set Name of Chord to root note + Key
                String ScaleName = rootNote + " " + patternName;
                //Get Int Array of Notes
                int[] pattern = ScalePatterns.get(patternName);
                //Get Index of the Root Name
                int rootNoteIndex = rootNotes.indexOf(rootNote);
                //Declare Name Array for storing Notes
                note[] scaleNotes = new note[pattern.length];
                //Iterate over notes in pattern
                for (int i = 0; i < pattern.length; i++) {
                    //Make a MIDI number starting at Octave 3 for the note by adding the index of the root note to the
                    //pattern number (-1 to start at 0 index) and adding 36 for 3 octaves
                    scaleNotes[i] = new note(((pattern[i] - 1) + rootNoteIndex) + 36);
                }
                scales.put(ScaleName, scaleNotes);
            }
        }
    }

    //Getters and Setters

    /**
     * Gets the Chords that match the provided root note.
     *
     * @param rootNote The root note that you want to find chords for.
     * @return Returns an ArrayList of Chord Names that match the root note provided.
     */
    private ArrayList<String> getRootNoteChords(String rootNote) {
        //Matches first note of Chord with root Name, if there is a match, return it.

        ArrayList<String> result = new ArrayList<>();

        chords.entrySet().forEach(chord ->
        {
            if (chord.getValue()[0].getName().equals(rootNote)) {
                result.add(chord.getKey());
            }
        });

        return result;
    }

    /**
     * Gets the Scales that match the provided root note.
     *
     * @param rootNote The root note that you want to find scales for.
     * @return Returns an ArrayList of Scale Names that match the root note provided.
     */
    private ArrayList<String> getRootNoteScales(String rootNote) {
        //Matches first note of Chord with root Name, if there is a match, return it.

        ArrayList<String> result = new ArrayList<>();

        scales.entrySet().forEach(scale ->
        {
            if (scale.getValue()[0].Name.equals(rootNote)) {
                result.add(scale.getKey());
            }
        });

        return result;
    }

    /**
     * Gets the Notes of the Chord or Scale name that is provided.
     *
     * @param Name The Name of the Chord or Scale that you want to find Notes for.
     * @param chords List of all Chords.
     * @param scales List of all Scales.
     * @return Returns an Array of all the notes that are contained in the Chord or Scale provided.
     * @see note
     */
    static note[] getChordScale(String Name, Map<String, note[]> chords, Map<String, note[]> scales) {
        note[] result = chords.get(Name);

        if (result == null) {
            result = scales.get(Name);
        }

        return result;
    }

    /**
     * Gets the Name of the currently selected Chord or Scale.
     * @return Name of the currently selected Chord or Scale.
     */
    static String getSelectedChordScaleName() {
        return selectedChordScaleName;
    }

    /**
     * Sets the Name of the currently selected Chord or Scale.
     */
    private static void setSelectedChordScaleName(String Name) {
        selectedChordScaleName = Name;
    }

    /**
     * Gets the List of all Chords
     * @return List of all Chords
     */
    static Map<String, note[]> getChords() {
        return chords;
    }

    /**
     * Gets the List of all Scales
     * @return List of all Scales
     */
    static Map<String, note[]> getScales() {
        return scales;
    }

    /**
     * Sets the Midi Polling Time
     * @param midiPollingTime The Midi Polling Time that is wanted.
     */
    public static void setMidiPollingTime(int midiPollingTime) {
        mainUI.midiPollingTime = midiPollingTime;
    }


}

class pianoDisplay extends JPanel {

    /**
     * Draws the Piano Display using the current Chord or Scale and the MIDI Notes
     * @param graphics The Graphics Object of the JFrame
     */
    @Override
    protected void paintComponent(Graphics graphics) {
        super.paintComponent(graphics);
        //Get Properties
        int totalWidth = this.getPreferredSize().width;
        int totalHeight = this.getPreferredSize().height;

        //Get notes of Currently Selected Chord/Scale, and the Current Midi Input
        note[] currentChordScaleNotes = mainUI.getChordScale(mainUI.getSelectedChordScaleName(), mainUI.getChords(), mainUI.getScales());

        ArrayList<note> currentMidiNotes = new ArrayList<>();

        if (mainUI.MIDIAvailable && mainUI.MIDIStarted) {
            currentMidiNotes = mainUI.mainMIDI.mainMidiInputReceiver.activeMidiNotes;
        }


        //Iterate over Root Notes
        for (String rootNote1 : mainUI.rootNotes) {
            //Iterate over Octaves
            for (int octave1 = 3; octave1 <= 4; octave1++) {
                note Note1 = new note(rootNote1, octave1);

                int keyPosition1;

                //If White Key (Natural Name)
                if (Note1.SharpFlatNoteIndex == 0) {
                    //Calculate Position
                    keyPosition1 = (totalWidth / 14) * (Note1.getNaturalNoteIndex() - 1);

                    if (Note1.Octave == 4) {
                        keyPosition1 = keyPosition1 + (totalWidth / 2) - 5;
                    }

                    //Draw shape
                    graphics.setColor(Color.white);
                    graphics.fillRect(keyPosition1, 0, totalWidth / 14, totalHeight);
                    graphics.setColor(Color.black);
                    graphics.drawRect(keyPosition1, 0, totalWidth / 14, totalHeight);

                    for (note currentChordScaleNote : currentChordScaleNotes) {
                        //If it is part of the current chord make it blue
                        if (note.Compare(currentChordScaleNote, Note1) == 3) {
                            graphics.setColor(Color.blue);
                            graphics.fillRect(keyPosition1, 0, totalWidth / 14, totalHeight);
                            graphics.setColor(Color.black);
                            graphics.drawRect(keyPosition1, 0, totalWidth / 14, totalHeight);
                        }
                    }

                    for (note currentMidiNote : currentMidiNotes) {
                        System.out.println("---------------");
                        System.out.println(currentMidiNote.toString());
                        System.out.println(Note1.toString());
                        System.out.println("---------------");
                        //If it is part of the current Midi input make it yellow
                        if (note.Compare(currentMidiNote, Note1) == 3) {
                            graphics.setColor(Color.yellow);
                            graphics.fillRect(keyPosition1, 0, totalWidth / 14, totalHeight);
                            graphics.setColor(Color.black);
                            graphics.drawRect(keyPosition1, 0, totalWidth / 14, totalHeight);
                        }
                    }
                }
            }

            for (String rootNote2 : mainUI.rootNotes) {
                for (int octave2 = 3; octave2 <= 4; octave2++) {
                    note Note2 = new note(rootNote2, octave2);

                    int keyPosition2;

                    //If Black Key (Sharp Or Flat)
                    if (Note2.getNaturalNoteIndex() == 0) {
                        //Calculate Position
                        keyPosition2 = ((totalWidth * (Note2.getSharpFlatNoteIndex() - 1)) / 14) + ((25 * totalWidth) / 448);

                        if (Note2.getSharpFlatNoteIndex() > 2) {
                            keyPosition2 += (totalWidth / 14);
                        }

                        if (Note2.Octave == 4) {
                            keyPosition2 = keyPosition2 + (totalWidth / 2) - 5;
                        }

                        graphics.setColor(Color.black);
                        graphics.fillRect(keyPosition2, 0, ((totalWidth / 28) - (totalWidth / 224)), totalHeight / 2);

                        for (note currentChordScaleNote : currentChordScaleNotes) {
                            //If it is part of the current chord make it blue
                            if (note.Compare(currentChordScaleNote, Note2) == 3) {
                                graphics.setColor(Color.blue);
                                graphics.fillRect(keyPosition2, 0, ((totalWidth / 28) - (totalWidth / 224)), totalHeight / 2);
                                graphics.setColor(Color.black);
                                graphics.drawRect(keyPosition2, 0, ((totalWidth / 28) - (totalWidth / 224)), totalHeight / 2);
                            }
                        }

                        for (note currentMidiNote : currentMidiNotes) {
                            //If it is part of the current Midi input make it yellow
                            if (note.Compare(currentMidiNote, Note2) == 3) {
                                graphics.setColor(Color.yellow);
                                graphics.fillRect(keyPosition2, 0, ((totalWidth / 28) - (totalWidth / 224)), totalHeight / 2);
                                graphics.setColor(Color.black);
                                graphics.drawRect(keyPosition2, 0, ((totalWidth / 28) - (totalWidth / 224)), totalHeight / 2);
                            }
                        }

                    }
                }
            }
        }
    }
}