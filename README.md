# README #

### What is this repository for? ###

This is a Piano Chord and Scale Library created for an A-Level Project. It can interface with a MIDI Keyboard to ensure the correct notes are being played.

### How do I get set up? ###

Just Download the Code and Compile it. It requires Java with Swing.